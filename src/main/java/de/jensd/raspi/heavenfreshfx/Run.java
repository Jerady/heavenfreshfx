package de.jensd.raspi.heavenfreshfx;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Run extends Application {

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void start(Stage stage) throws Exception {

        
        
        ResourceBundle resources = ResourceBundle.getBundle("i18n/messages");
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/mainpanel.fxml"), resources);

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/heavenfresh.css");

        stage.setTitle(resources.getString("app.title"));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
