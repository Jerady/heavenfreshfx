package de.jensd.raspi.heavenfreshfx;

import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.fxml.FXML;

public class MainPanelController {

    private static Logger logger = Logger.getLogger(MainPanelController.class.getName());

    private GpioAdapter gpioAdapter;

    @FXML
    ResourceBundle resources;

    @FXML
    public void initialize() {
        gpioAdapter = new GpioAdapter();
    }

    @FXML
    public void switchFlow() {
        logger.info("switchFlow()");
    }

    @FXML
    public void switchPower() {
        logger.info("switchPower()");
        //gpioAdapter.connect();

    }

    @FXML
    public void switchTimer() {
        logger.info("switchTimer()");

    }

}
